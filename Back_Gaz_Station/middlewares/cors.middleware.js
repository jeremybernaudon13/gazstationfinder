var express = require('express')
var cors = require('cors')
var app = express()


// Mise en place de Cors de maniere Asynchrone à l'aide d'une WhiteList

var whitelist = ['http://localhost:3000','http://34.78.53.100:3000','http://localhost:4200','http://localhost:80','http://localhost','http://34.76.134.175','http://34.76.134.175:80','*']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

module.exports= cors(corsOptions);
