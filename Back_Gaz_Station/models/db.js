const mongoose = require("mongoose");

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

//Mettre l'url de mongoDB a la place de la variable d'environnement pour docker: "mongodb://127.0.0.1:27017/gaz-pricing"

const url = process.env.MONGO_DB;

mongoose.connect(url, options);

mongoose.connection.on("connected", () => {
    console.log("Connection to database successfully established");
});

mongoose.connection.on("disconnected", () => {
    console.log("Disconnected from database");
});

module.exports = mongoose.connection;