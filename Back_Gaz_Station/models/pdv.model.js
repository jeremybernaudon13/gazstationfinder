const mongoose = require('mongoose');
const passportLocalMongoose = require("passport-local-mongoose");

const Schema = mongoose.Schema;

const pdvSchema = new Schema({
    latitude: Number,
    longitude: Number,
    address: String,
    zipCode: Number,
    city: String,
    price: [Number],
    gazType: [Number]
});


const pdv = mongoose.model("pdv", pdvSchema);

module.exports = pdv;