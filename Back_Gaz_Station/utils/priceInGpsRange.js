
const pdv = require("../models/pdv.model");
const geolib = require('geolib');
var db = require('../models/db.js');

function parseCoordLatitude(coord){
    if(String(coord).charAt(0) == "-" || String(coord).charAt(0) == "0"){
        newCoord = "Error in the gouvernemental file";
    }
    else{
        newCoord = String(coord).substr(0,2) + "." + String(coord).substr(2,5);
    }
    return newCoord;
}

function parseCoordLongitude(coord){
    if(String(coord).charAt(0) == "-"){
        newCoord = String(coord).substr(0,2) + "." + String(coord).substr(2,5);
    }
    else{
        newCoord = String(coord).substr(0,1) + "." + String(coord).substr(1,5);
    }
    return newCoord;
}

exports.getLowerPrice = (latitude, longitude, range, gazType, callback) => {

    var minPrice = 1000000;
    var tempPrice;
    var minPriceLocation;
    var stationIsFind = false;

    var currentCoord = {
        latitude: Number(latitude),
        longitude: Number(longitude)
    };
    
    db.collection("pdvs").find().toArray(function (error, results) {
        if (error) throw error;
        for(i=1; i<(results.length); i++){
            try{
                var tempCoord = {
                    latitude: Number(parseCoordLatitude(results[i].latitude)),
                    longitude: Number(parseCoordLongitude(results[i].longitude))
                };

                /*if(Number(geolib.getDistance(currentCoord,tempCoord)) <= (range * 1000)){
                    for(j=0; j<results[i].gazType.length; j++){
                        if(results[i].gazType[j] == gazType){

                            if(String(results[i].price[j]).length == 4){
                                tempPrice = results[i].price[j].toString();
                                tempPrice = tempPrice.substring(0,1) + "." + tempPrice.substring(1,4);
                            }
                            else if(String(results[i].price[j]).length == 3){
                                tempPrice = Number("0." + results[i].price[j]);
                            }
                            
                            if(tempPrice < minPrice){
                                minPrice = tempPrice;
                                minPriceLocation = results[i];
                            }
                        }
                    }
                }*/
                if(Number(geolib.getDistance(currentCoord,tempCoord)) <= (range * 1000)){
                    for(j=0; j<results[i].gazType.length; j++){
                        if(results[i].gazType[j] == gazType){
                            stationIsFind = true;
                            tempPrice = Number(results[i].price[j].toString());
                            
                            if(tempPrice < minPrice){
                                minPrice = tempPrice;
                                minPriceLocation = results[i];
                            }
                        }
                    }
                }
                
            }catch (error){
                console.log(error);
            }
        }

        var reponse = [];

        if(stationIsFind){
            console.log(JSON.stringify(minPriceLocation.address) + ", " + JSON.stringify(minPriceLocation.city) + ", " + JSON.stringify(minPriceLocation.zipCode));
            console.log(minPrice);
            reponse.stationIsFind = stationIsFind;
            reponse.phrase = "Meilleur prix : " + minPrice + " euro par litre à " + JSON.stringify(minPriceLocation.address) + ", " + JSON.stringify(minPriceLocation.city) + ", " + JSON.stringify(minPriceLocation.zipCode);
            reponse.station = minPriceLocation;
        }
        else{
            console.log("Station non trouvée dans le périmètre donné");
            reponse.stationIsFind = stationIsFind;
            reponse.phrase = "Station non trouvée dans le périmètre donné";
            reponse.station = null
        }
        

        callback(reponse);
    });
}