const mongoose = require('mongoose');
const pdv = require('../models/pdv.model');
var iconv = require('iconv-lite');
const download = require('download');
const fs = require('fs');
const xml2js = require('xml2js');
var date = require('./date');
var db = require('../models/db.js');


function toUTF8(temp) {
    // convert from iso-8859-1 to utf-8
    var ic = new iconv.Iconv('ISO-8859-1', 'UTF-8');
    var buf = ic.convert(temp);
    return buf.toString('utf-8');
}

try{
    db.collection('pdvs').drop();
    console.log("Collection droped") 

    try{
        (async () => {
            //download gaz datas file
            //await download("https://donnees.roulez-eco.fr/opendata/jour", "ressources", { extract: true });
            await download("https://donnees.roulez-eco.fr/opendata/instantane", "ressources", { extract: true });
            console.log("file downloaded");
        
            //parsing gaz datas file
            //fs.readFile(__dirname + "/../ressources/PrixCarburants_quotidien_" + date.yyyymmdd() + ".xml", function(err, data){

            
            var input = fs.readFileSync(__dirname + "/../ressources/PrixCarburants_instantane.xml", {encoding: "binary"});

            var iconv = require('iconv-lite');
            var output = iconv.decode(input, "ISO-8859-1");

            fs.writeFileSync(__dirname + "/../ressources/PrixCarburants_instantane.xml", output);
            

            fs.readFile(__dirname + "/../ressources/PrixCarburants_instantane.xml", function(err, data){

                if (err) throw new Error(err);

                const parser = new xml2js.Parser();
        
                parser.parseStringPromise(data)
                    .then(function (res){
        
                        console.log(res.pdv_liste.pdv[0].prix.length);
        
                        for(i=0; i < res.pdv_liste.pdv.length; i++){
        
                            var pdvTemp = new pdv({
                                latitude: res.pdv_liste.pdv[i].$.latitude,
                                longitude: res.pdv_liste.pdv[i].$.longitude,
                                //address: toUTF8(res.pdv_liste.pdv[i].adresse[0]),
                                //city: toUTF8(res.pdv_liste.pdv[i].ville[0]),
                                address: res.pdv_liste.pdv[i].adresse[0],
                                city: res.pdv_liste.pdv[i].ville[0],
                                zipCode: res.pdv_liste.pdv[i].$.cp
                                                     
                            });
        
                            var subPriceList = [];
                            var subGazTypeList = [];
        
                            try{
                                for(j=0; j < res.pdv_liste.pdv[i].prix.length; j++){
                                    subPriceList.push(res.pdv_liste.pdv[i].prix[j].$.valeur);
                                    subGazTypeList.push(res.pdv_liste.pdv[i].prix[j].$.id);
                                }          
        
                            }  catch (error) {
                               // console.log("Gaz Station is Closed");
                            }
        
                            pdvTemp.price = subPriceList;
                            pdvTemp.gazType = subGazTypeList;
        
                            //console.log("OK!");
        
                            pdvTemp.save();
                        }
                        console.log("Download and importation success ! you can use the API service !");
                    })
                    .catch(function (err) {
                        console.log(err);
                })
            });
             
        })()
    }
    catch (error){
        console.log("Download or parsing problem")
    }

}catch (error){
    console.log("Drop Collection problem");
}


