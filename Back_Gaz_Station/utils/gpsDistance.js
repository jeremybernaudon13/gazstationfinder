const pdv = require("../models/pdv.model");
const geolib = require('geolib');
var db = require('../models/db.js');

function parseCoordLatitude(coord){
    if(String(coord).charAt(0) == "-" || String(coord).charAt(0) == "0"){
        newCoord = "Error in the gouvernemental file";
    }
    else{
        newCoord = String(coord).substr(0,2) + "." + String(coord).substr(2,5);
    }
    return newCoord;
}

function parseCoordLongitude(coord){
    if(String(coord).charAt(0) == "-"){
        newCoord = String(coord).substr(0,2) + "." + String(coord).substr(2,5);
    }
    else{
        newCoord = String(coord).substr(0,1) + "." + String(coord).substr(1,5);
    }
    return newCoord;
}

exports.getCloserStation = (latitude, longitude, callback) => {

    var minDistance;
    var minDistanceLocation;
    var stationIsFind = false;

    var targetCoord = {
        latitude: Number(latitude),
        longitude: Number(longitude)
    };
    
    db.collection("pdvs").find().toArray(function (error, results) {
        if (error) throw error;

        var minDistanceCoord = {
            latitude: Number(parseCoordLatitude(results[0].latitude)),
            longitude: Number(parseCoordLongitude(results[0].longitude))
        };
        minDistance = Number(geolib.getDistance(targetCoord, minDistanceCoord));

        minDistanceLocation = results[0];
        stationIsFind = true;

        for(i=1; i<(results.length); i++){
            try{
                var tempCoord = {
                    latitude: Number(parseCoordLatitude(results[i].latitude)),
                    longitude: Number(parseCoordLongitude(results[i].longitude))
                };

                var distanceTemp = Number(geolib.getDistance(targetCoord, tempCoord));

                if(distanceTemp < minDistance){
                    minDistanceLocation = results[i];
                    minDistance = distanceTemp;
                }

            }catch (error){
                console.log("distance comparison error")
            }
        }

    var response = [];
console.log(stationIsFind)
  if(stationIsFind){
    console.log(minDistanceLocation);
       response.stationIsFind = stationIsFind;
       response.station = minDistanceLocation;
  }
  else {
      console.log("test")
  }
  callback(response)
    });
}