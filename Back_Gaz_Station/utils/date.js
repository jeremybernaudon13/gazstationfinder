exports.yyyymmdd = () => {
    var x = new Date();
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    (d.length == 1) && (d = '0' + d);
    (m.length == 1) && (m = '0' + m);
    
    if(d == '01'){
        switch(m){
            case '01':
                m = '12';
                d = '31';
                y = (y-1);
                break;
            case '02':
                m = '01';
                d = '31';
                break;
            case '03':
                if((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)){
                    m = '02';
                    d = '29';
                }
                else{
                    m = '02';
                    d = '28';
                }
                break;
            case '04':
                m = '03';
                d = '31';
                break;
            case '05':
                m = '04';
                d = '30';
                break;
            case '06':
                m = '05';
                d = '31';
                break;
            case '07':
                m = '06';
                d = '30';
                break;
            case '08':
                m = '07';
                d = '31';
                break;
            case '09':
                m = '08';
                d = '31';
                break;
            case '10':
                m = '09';
                d = '30';
                break;
            case '11':
                m = '10';
                d = '31';
                break;
            case '12':
                m = '11';
                d = '30';
        }

        var yyyymmdd = y + m + d;
    }
    else{
        var yyyymmdd = y + m + (d - 1);
    }
    return yyyymmdd;
}
