# API GAZ Pricing M2 2020-2021

## Ce service fournit une API permettant de récupérer différentes informations relatives aux points de vente de carburant en France.

## Cette première version de notre API prend en charge:

### Pour les utilisateurs:

- La création d'utilisateurs
- L'authentification d'utilisateurs
- Recupération de la liste de tous les utilisateurs avec leurs attributs
- Récupération des attributs d'un utilisateur spécifique avec son ID
- Modification des attributs d'un utilisateur spécifique avec son ID
- Suppression d'un utilisateur spécifique avec son ID

### Pour les points de vente de carburant:

- Récupération de l'adresse du point de vente de caburant le plus proche en fonction des coordonnées GPS données l'utilisateur
- Recherche du point de vente de carburant le moins cher dans un rayon de X kilomètres en fonction des coordonnées GPS, le type de carburant et du rayon de recherche donné l'utilisateur
  et récupération de l'adresse du point de vente et du prix du carburant recherché


## Détail de chaque fonctionnalités:

### - La création d'utilisateurs:
Pour créer un utilisateur, il faut envoyer une requête POST à l'adresse https://localhost:3000/users/register contenant un fichier JSON avec le pseudo de l'utilisateur, son prénom, son nom et son mot de passe.

    {
	    "username": "terminator",
	    "firstName": "arnold ",
	    "lastName": "schwarzenegger",
	    "password": "azerty123"
    }

### - L'authentification d'utilisateurs:
Pour qu'un utilisateur s'authentifie, il doit envoyer une requête POST à l'adresse https://localhost:3000/users/login contenant un fichier JSON avec le pseudo de l'utilisateur et son mot de passe.

    {
        "username": "terminator",
	    "password": "azerty123"
    }
Une fois la requête envoyée, l'API va renvoyer un JSON contenant le token de connexion de l'utilisateur pour lui permettre d'utiliser les fonctionnalités suivantes.

    {
        "success": true,
        "status": "You are succesfully logged in",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZWU3Y2Q5NDU0ODZlZDQyZGNiMWIzMWUiLCJpYXQiOjE1OTIyNDk3NjEsImV4cCI6MTU5MjMzNjE2MX0.KmofJq-uupIBWPRg9cUrxlJk4c5vj2PCXriIprvE0Gk",
        "user": {
            "username": "terminator",
            "firstName": "arnold ",
            "lastName": "schwarzenegger"
        }
    }

### - Recupération de la liste de tous les utilisateurs avec leurs attributs (ADMIN)
Cette fonctionnalité est réservée uniquement aux utilisateurs administrateurs. Pour qu'un utilisateur devienne administrateur, il faut modifier l'attribut booleen "admin" dans la base de donnée au niveau du document concernant l'utilisateur en question.
Cela permet une meilleure sécurisation de l'attribution du rôle administrateur.

Pour récupérer la liste de tous les utilisateurs de l'API avec leurs attributs, il faut envoyer une requête GET à l'adresse https://localhost:3000/users/ on obtient en réponse un fichier JSON de ce type:

    [
        {
            "admin": false,
            "_id": "5ee7cc5a5486ed42dcb1b31d",
            "username": "toto1",
            "firstName": "toto1",
            "lastName": "toto1",
            "createdAt": "2020-06-15T19:30:34.667Z",
            "updatedAt": "2020-06-15T19:30:34.667Z",
            "__v": 0
        },
        {
            "admin": true,
            "_id": "5ee7cd945486ed42dcb1b31e",
            "username": "terminator",
            "firstName": "arnold ",
            "lastName": "schwarzenegger",
            "createdAt": "2020-06-15T19:35:49.037Z",
            "updatedAt": "2020-06-15T19:35:49.037Z",
            "__v": 0
        }
    ]


### - Récupération des attributs d'un utilisateur spécifique avec son ID (ADMIN)
Pour récupérer les attributs d'un utilisateur spécifique, il faut envoyer une requête GET à l'adresse https://localhost:3000/users/ID_User_ciblé. par exemple: ../users/5ee7cc5a5486ed42dcb1b31d
L'API nous renvoie la réponse sous la forme d'un fichier JSON:

    {
        "admin": false,
        "_id": "5ee7cc5a5486ed42dcb1b31d",
        "username": "toto1",
        "firstName": "toto1",
        "lastName": "toto1",
        "createdAt": "2020-06-15T19:30:34.667Z",
        "updatedAt": "2020-06-15T19:30:34.667Z",
        "__v": 0
    }


### - Modification des attributs d'un utilisateur spécifique avec son ID (ADMIN)
Pour modifier les attributs d'un utilisateur spécifique, il faut envoyer une requête PUT à l'adresse https://localhost:3000/users/ID_User_ciblé. contenant le ou les nouveaux attributs:

    {
        "username": "toto1"
    }
l'API nous répond en renvoyant tous les attributs de l'utilisateur en question avec ses données mises à jour:

    {
        "admin": false,
        "_id": "5ee7cc5a5486ed42dcb1b31d",
        "username": "toto2",
        "firstName": "toto1",
        "lastName": "toto1",
        "createdAt": "2020-06-15T19:30:34.667Z",
        "updatedAt": "2020-06-15T20:09:56.056Z",
        "__v": 0
    }

### - Supression d'un utilisateur spécifique avec son ID (ADMIN)
Pour supprimer un utilisateur, il faut envoyer une requête DELETE à l'adresse https://localhost:3000/users/ID_User_ciblé.
L'API nous confirme la suppression de l'utilisateur en nous renvoyant les attributs de l'utilisateur supprimé:

    {
        "admin": false,
        "_id": "5ee7cc5a5486ed42dcb1b31d",
        "username": "toto2",
        "firstName": "toto1",
        "lastName": "toto1",
        "salt": "7e1f9f403d15943f8cd9bc5c1d1a2b1d25919249b4d4663ac50fa9d2151ab38a",
        "hash": "bc3f35706dd52a0d34a83999e6655b107a440a9a1c517a677c25d0f6c6848e834648be18c367930d541737bebc60c352ebdf74d46336a37176fd2588d60f37183a80664ceceaf436ed1e6332dd97c558109b1d9809b7d477caaa99d43f58ebc3cb4b7d79770ca69d2aa1e197133acf7eeb1418cce394879d46e31dfa1d3c48358df1f5629b0100d3f8cc0ab7a15575dc1f23ac47c2c4356c2f29f79420cb311323c180cc9bcfb0e21bb3becfbdbad001f8cdebacf063e12944d5164bc1119d300ef59928f787d43e9069c2aacad27b8ebc7e8802f3c2c77748b4d43dc1d21cdfcfa0016104196fe8150457055ab74517a6d53bde8eb64bc2d722b6f270f48de9d08f902785b0453a8cade380d1a388b84fb5000460aab17129f91ad6b004c9c4875b40a40800cf3cb367195160263d60560ef5465c4b30329da28578114de2d71ff30dd6daee4b3169b7242c5c318f1f2046a23537b60664b41c4700799f7ce311aa5d8a1ccfade6671723172d86cbee6efa155551c0d9033842468e37731d102270fad2afb126e1dee73be489239c74907c4860e0a952e3a23db4a189c1ce45f2cf691e8960469bb1439fec6b13ef0634f104f64964da498c5313a7b2e542510470c18a562c6b3927201e9760fcaadf7a76f2ad534af869e2eaeeba4cbf0422dd52ab9cb68248b2156f5235becfeb7ab0c303e0ed7d040f2f352901b0b6588c",
        "createdAt": "2020-06-15T19:30:34.667Z",
        "updatedAt": "2020-06-15T20:09:56.056Z",
        "__v": 0
    }


### - Récupération de l'adresse du point de vente de caburant le plus proche en fonction des coordonnées GPS données l'utilisateur
Pour récupérer l'adresse du point de vente de carburant le plus proche, un utilisateur devra envoyer une requête POST à l'adresse https://localhost:3000/pdv/gpsDistance contenant un fichier JSON 
avec la latitude et la longitude de l'endroit où se trouve l'utilisateur:

    {
	    "latitude": 48.89227,
	    "longitude": 2.23945
    }
L'API nous répond alors en nous envoyant l'addresse du point de vente de carburant le plus proche:

    {
        "success": true,
        "address": 1 RUE DE STRASBOURG, COURBEVOIE, 92400"
    }


### - Recherche du point de vente de carburant le moins cher dans un rayon de X kilomètres en fonction des coordonnées GPS, le type de carburant et du rayon de recherche donné l'utilisateur et récupération de l'adresse du point de vente et du prix du carburant recherché

Pour lancer une recherche, l'utilisateur doit envoyer un JSON à l'adresse https://localhost:3000/pdv/priceGps contenant les coordonnées GPS latitude et longitude de l'endroit où se trouve l'utilisateur mais également la taille du rayon de recherche en kilomètre
ainsi que le type de carburant recherché avec un code spécifique. voici à quoi corespondent les codes disponibles:
1 --> Gazole
2 --> SP95
3 --> E85
4 --> GPLc
5 --> E10
6 --> SP98

par exemple, si l'utilisateur envoie le fichier JSON suivant en requête POST:
    
    {
	    "latitude": 48.89227,
	    "longitude": 2.23945,
        "range": 5,
        "gazType": 6
    }

L'API répond à l'utilisateur en lui envoyant l'adresse de la station où le prix le plus bas a été trouvé: 

    {
        "success": true,
        "priceAndAddress": "Best price: 1.337 euros per litter at 108 Boulevard ï¿½mile Zola, HOUILLES, 78800"
    }

## Utiliser l'API avec Postman

Pour utiliser l'API avec Postman, il faut veiller à aller dans File --> Settings et de décocher la case "SSL certification verification"
