const express = require("express");
var controller = require("../controller/pdv.controller");
const router = express.Router();
var authenticate = require("../middlewares/authenticate.middleware");
router.route("/gpsDistance").post(authenticate.isAuthenticated, authenticate.isUser, controller.gpsDistance);

router.route("/priceGps").post(authenticate.isAuthenticated, authenticate.isUser, controller.priceGps);

module.exports = router;