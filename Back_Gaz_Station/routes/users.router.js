const express = require("express");
var passport = require("passport")
var controller = require("../controller/users.controller");
const router = express.Router();
var authenticate = require("../middlewares/authenticate.middleware");


router.route("/login").post(passport.authenticate("user", {session: false}), controller.login);
router.route("/register").post(controller.register);
router.route("/").get(authenticate.isAuthenticated, authenticate.isUser, authenticate.isAdmin, controller.getAllUsers);
router.route("/:id").get(authenticate.isAuthenticated, authenticate.isUser, authenticate.isAdmin, controller.getOneUsers);
router.route("/:id").delete(authenticate.isAuthenticated, authenticate.isUser, authenticate.isAdmin, controller.deleteOneUsers);
router.route("/:id").put(authenticate.isAuthenticated, authenticate.isUser,authenticate.isAdmin, controller.updateOneUsers);

module.exports = router;