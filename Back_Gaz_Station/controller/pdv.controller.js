const User = require("../models/pdv.model");
const gps = require("../utils/gpsDistance");
const priceGps = require("../utils/priceInGpsRange");

var controller = {

    gpsDistance: (req, res, next) => {
      res.statusCode = 200;
      res.setHeader("Content-Type", "application/json");
      gps.getCloserStation(req.body.latitude, req.body.longitude, function (data){
        res.json({
          success: true,
          stationIsFind: data.stationIsFind,
          station: data.station
        });
      })
    },

    priceGps: (req, res, next) => {
      res.statusCode = 200;
      res.setHeader("Content-Type", "application/json");
      priceGps.getLowerPrice(req.body.latitude, req.body.longitude, req.body.range, req.body.gazType, function (data){
        res.json({
          success: true,
          stationIsFind: data.stationIsFind,
          phrase: data.phrase,
          station: data.station
        });
      })
    },
};

module.exports = controller;