// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlBack: "http://34.78.53.100:3000",
  mapbox: {
    accessToken: 'pk.eyJ1IjoidGFnOTAwMCIsImEiOiJja2p1bjMzZWgwMG04MndzZHV3OGZmdnFrIn0.8jlvKjKyOlc7wgIVFTE8iA'
  }
  
}; 

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
