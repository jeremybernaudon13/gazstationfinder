import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JwtModule } from "@auth0/angular-jwt";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, JsonpClientBackend } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { GpsDistanceComponent } from './gps-distance/gps-distance.component';
import { PriceGpsComponent } from './price-gps/price-gps.component';
import { AccueilComponent } from './accueil/accueil.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {environment} from '../environments/environment'

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    GpsDistanceComponent,
    PriceGpsComponent,
    AccueilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AccordionModule.forRoot(),
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return sessionStorage.getItem("userToken");
        },
        allowedDomains: [environment.urlBack],
        disallowedRoutes: [environment.urlBack + "/register", environment.urlBack + "/login"],
        skipWhenExpired: true
      },
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
