import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as mapboxgl from 'mapbox-gl';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-gps-distance',
  templateUrl: './gps-distance.component.html',
  styleUrls: ['./gps-distance.component.css']
})
export class GpsDistanceComponent implements OnInit {

  form: FormGroup;
  map: mapboxgl.Map;
  markerYou : mapboxgl.Marker;
  markerStation : mapboxgl.Marker;
  style = 'mapbox://styles/mapbox/streets-v11';
 
  constructor(
    public fb: FormBuilder,
    private http: HttpClient
  ) {
    this.form = this.fb.group({
      latitude: [null],
      longitude: [null]
    });
  }

  ngOnInit(): void {
   
    (mapboxgl as any).accessToken = environment.mapbox.accessToken;
      this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      center: [2.345650730357267, 48.85593021231582],
      zoom: 11.5
      
    });

    this.map.addControl(new mapboxgl.NavigationControl());
    
  }

  
  
 
  public data :any;
  public isDataEmpty : boolean = true; 
  public isParse : boolean = true; 


  submitForm() {
    console.log(this.form.getRawValue());
    
    this.http.post(environment.urlBack + '/pdv/gpsDistance', this.form.getRawValue()).subscribe(
      (response) => {
      this.data = response;
      this.isDataEmpty = false; 
      this.isParse = false ;
      
      this.addMarkerYou(this.map, Number(this.form.getRawValue().longitude), Number(this.form.getRawValue().latitude), this.markerYou);
      this.addMarkerStation(this.map, Number(this.parseCoordLongitude(this.data.station.longitude)), Number(this.parseCoordLatitude(this.data.station.latitude)), this.markerStation);
  
     //Fly To 
      var mapHt = this.map;
      var longi = this.getLongYou();
      var lati  = this.getLatYou();

      
      document.getElementById('fly').addEventListener('click', function () {
        mapHt.flyTo({
        center: [longi, lati],
        essential: true 
        });
      });
    },
      (error) => {
        alert("Il faut d'abord vous connecter pour utiliser cette fonctionalité");
        console.log(error.message);
      }
    )
    

  }


  
/** fonctions get pour les elements du json afin de 
 * faire leur affichage comme on veut en HTML */


get getLatitude (){

  if (this.isParse == false){
  if (this.isDataEmpty == false){
    if (this.data.stationIsFind== true){
        return (this.data && this.parseCoordLatitude(this.data.station.latitude))  ? this.parseCoordLatitude(this.data.station.latitude) : null  
    }else{
        return null;
      }
    }
  }
}

get getLongitude (){
  if (this.isParse == false){
    if (this.isDataEmpty == false){
      if (this.data.stationIsFind== true){
          return (this.data && this.parseCoordLongitude(this.data.station.longitude) ) ? this.parseCoordLongitude(this.data.station.longitude) : null ;
     
      }else{
          return null;
        }
      }
  }
}

get getPrix (){

  if (this.isDataEmpty == false){
    if (this.data.stationIsFind== true){
      return (this.data && this.data.station.price) ? this.data.station.price : null ;
    }else{
        return null;
      }
    }
}
get getGazType (){

  if (this.isDataEmpty == false){
    if (this.data.stationIsFind== true){
      return (this.data && this.data.station.gazType) ? this.data.station.gazType : null ; 
    }else{
        return null;
      }
    }
 
}

get getAdresse (){
  if (this.isDataEmpty == false){
    if (this.data.stationIsFind== true){
      return ((this.data && this.data.station.address) ? this.data.station.address : null) + ' at ' + ((this.data && this.data.station.city) ? this.data.station.city : null) + ' ' + ((this.data && this.data.station.zipCode) ? this.data.station.zipCode : null) ;  
    }else{
        return null;
      }
    }
}
  /** */
  latYou = null;
  longYou = null;
  addMarkerYou(map, longitude, latitude, markerYou) {
    var el = document.createElement('div');
    this.latYou = latitude;
    this.longYou=longitude; 
    el.className = 'markerYou';
    if (markerYou === undefined ){
      this.markerYou = new mapboxgl.Marker(el, {offset: [0, -50/2]}).setLngLat([this.longYou, this.latYou]).addTo(map);
    }else{
      this.markerYou.setLngLat([this.longYou, this.latYou])
    }
  }

  getLatYou(){
    return this.latYou;
  }
  getLongYou(){
    return this.longYou;
  }
  addMarkerStation(map, longitude, latitude, markerStation) {
    //new mapboxgl.Marker().setLngLat([longitude, latitude]).addTo(map);
    var el = document.createElement('div');
    el.className = 'markerStation';
      if (markerStation === undefined ){
        this.markerStation = new mapboxgl.Marker(el, {offset: [0, -50/2]}).setLngLat([longitude, latitude]).addTo(map);
      }else{
        this.markerStation.setLngLat([longitude, latitude])
      }
    
    }
    
 



  parseCoordLatitude(coord){
    var newCoord;
    var coupe = String(coord).split('');
    coupe.splice(coupe.indexOf("."),1);
    coord = coupe.join(''); 

    if(String(coord).charAt(0) == "-" || String(coord).charAt(0) == "0"){
        newCoord = "Error in the gouvernemental file";
    }
    else{
        newCoord = String(coord).substr(0,2) + "." + String(coord).substr(2,5);
    }
    if (newCoord.endsWith(".")) {
      newCoord = newCoord.slice(0, -1)
    }
    return newCoord;
  }
  
  parseCoordLongitude(coord){
    var newCoord;
    var coupe = String(coord).split('');
    coupe.splice(coupe.indexOf("."),1);
    coord = coupe.join(''); 

    if(String(coord).charAt(0) == "-"){
        newCoord = String(coord).substr(0,2) + "." + String(coord).substr(2,5);
    }
    else{
        newCoord = String(coord).substr(0,1) + "." + String(coord).substr(1,5);
    }
    if (newCoord.endsWith(".")) {
      newCoord = newCoord.slice(0, -1)
    }
  
    return newCoord;
  }


 

  

}
