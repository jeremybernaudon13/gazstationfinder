import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    public fb: FormBuilder,
    private http: HttpClient,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: [null],
      password: [null]
    })
  }

  ngOnInit(): void {
  }

  submitForm() {
    console.log(this.form.getRawValue());
    
    this.http.post(environment.urlBack + '/users/login', this.form.getRawValue()).subscribe(
      (response) => {
        console.log(response['token']);
        sessionStorage.setItem("userToken", response['token']);
        this.router.navigate(["/"]);
      },
      (error) => {
        console.log(error.message);
        alert("Erreur lors de la connexion de l'utilisateur, etes-vous inscrit sur notre site ?");
      }
    )
  }

}
