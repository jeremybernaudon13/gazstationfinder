import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { AuthenticationGuard, GuestGuard } from './authentication.guard';
import { GpsDistanceComponent } from './gps-distance/gps-distance.component';
import { LoginComponent } from './login/login.component';
import { PriceGpsComponent } from './price-gps/price-gps.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'gpsDistance', component: GpsDistanceComponent},
  {path: 'priceGps', component: PriceGpsComponent},
  {path: '', component: AccueilComponent}
];

//need fixing

/*const routes: Routes = [
  {path: 'register', canActivate:[GuestGuard], component: RegisterComponent},
  {path: 'login', canActivate:[GuestGuard], component: LoginComponent},
  {path: 'gpsDistance', canActivate:[AuthenticationGuard], component: GpsDistanceComponent},
  {path: 'priceGps', canActivate:[AuthenticationGuard], component: PriceGpsComponent}
];*/

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
