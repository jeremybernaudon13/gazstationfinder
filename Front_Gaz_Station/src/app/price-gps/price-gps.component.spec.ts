import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceGpsComponent } from './price-gps.component';

describe('PriceGpsComponent', () => {
  let component: PriceGpsComponent;
  let fixture: ComponentFixture<PriceGpsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PriceGpsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceGpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
