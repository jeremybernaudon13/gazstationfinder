import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FrontAPP';
  LoggedIn= false;

  ngOnInit(){
    this.isLoggedIn()
  }

  isLoggedIn(){
    if(sessionStorage.getItem("userToken") !== null){
      this.LoggedIn = true;
    } else{
      this.LoggedIn = false;
    }
  }

  disconnect(){
    sessionStorage.removeItem("userToken");
    this.LoggedIn = false;
    console.log(sessionStorage);
    location.reload();
  }

}

