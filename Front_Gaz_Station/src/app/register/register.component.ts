import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  form: FormGroup;
  userToken: [null];

  constructor(
    public fb: FormBuilder,
    private http: HttpClient,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: [null],
      firstName: [null],
      lastName: [null],
      password: [null]
    })
  }

  ngOnInit(): void {
  }

  submitForm() {
    console.log(this.form.getRawValue());
    
    this.http.post(environment.urlBack + '/users/register', this.form.getRawValue()).subscribe(
      (response) => {
        console.log(response)
        this.router.navigate(["/login"]);
      },
      (error) => {
        console.log(error)
        alert("Erreur lors de l'inscription de l'utilisateur");
      }
    )
  }

}
